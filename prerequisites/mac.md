
# วิธีติดตั้ง Docker Desktop 🐳 บน MacOS 🍎

1. ทำการเข้าไปยังเว็บไซต์ https://docs.docker.com/desktop/install/mac-install/ <br>
2. ทำการเลือกตัวติดตั้งให้ตรงกับ CPU ของท่าน โดย Apple Silicon จะหมายถึง Apple devices เวอร์ชั่นใหม่ที่เป็นตะกูล ARM [ CPU: Apple M1-M4 ] <br>
  <img  src="https://gitlab.com/thanakorn_got/gitlab-cicd-training/-/raw/main/image/Docker-typc.png?ref_type=heads" border="0" /> <br>

3. ทำเปิดไฟล์ติดตั้งที่ดาวน์โหลดมา <br>
4. ทำการลากไอคอน Docker ไปยัง แฟ้ม Applications ตามภาพ <br>
  <img  src="https://gitlab.com/thanakorn_got/gitlab-cicd-training/-/raw/main/image/move-icon-docker-mac.png?ref_type=heads" border="0" width="500" /> <br>
5. ทำการเปิด Docker  <br>
  <img  src="https://gitlab.com/thanakorn_got/gitlab-cicd-training/-/raw/main/image/open-docker-mac.png?ref_type=heads" border="0" width="500" /> <br>
6. Docker จะขึ้น Agreement ให้ยอมรับ ทำการกด Accept <br>
7. ทำการเลือก Use recommended settings แล้วกด Finish และทำการไส่รหัสผ่านของเครื่อง Macos ตามภาพ <br>
  <img  src="https://gitlab.com/thanakorn_got/gitlab-cicd-training/-/raw/main/image/userecomman-docker.png?ref_type=heads" border="0" width="800" /> <br>
8. หากขึ้นตามภาพแสดงว่าการติดตั้ง Docker เสร็จสิ้น ✨ <br>
  <img  src="https://gitlab.com/thanakorn_got/gitlab-cicd-training/-/raw/main/image/Done-macos-docker.png?ref_type=heads" border="0" width="800" /> <br>

### ทำการทดลองสร้าง Container

  

เปิด Terminal run:

```sh

docker  run  -d  -p  80:80  docker/getting-started

```

เสร็จแล้วทดสอบเข้า URL http://localhost 
---------------------------
