# How to install docker and kubernetes 

## วิธีติดตั้ง Docker Desktop บน Windows 

ทำการเปิดใช้งาน Windows Subsystem for Linux (WSL) 

#### สำหรับ Windows 10 version 2004 (Build 19041 ขึ้นไป) หรือ Windows 11 
 

#### เปิด PowerShell as Administrator and run: 

```wsl --install ```

เปิด PowerShell as Administrator and run: 

```shell 
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart 
``` 

#### ทำการเปิดใช้งาน Virtual Machine feature <br> 

ใน PowerShell run: 

```shell 
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart 
``` 

#### ตั้งค่า default wsl ของเราให้เป็น version 2

```wsl --set-default-version 2```

(Optional) ทำการเปิดใช้งาน Virtual Machine feature 

ไปที่ Add optional feature -> add window feature 
 
<img src="../image/add-windows-wsl-feature.png" alt="drawing" width="400"/>


#### สำหรับ windows version ต่ำกว่า Windows 10 version 2004 (Build 19041) 

#### ทำการเปิดใช้งาน Windows Subsystem for Linux (WSL) <br> 

เปิด PowerShell as Administrator and run: 

```shell 
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart 
``` 

#### ทำการเปิดใช้งาน Virtual Machine feature <br> 

ใน PowerShell run: 

```shell 
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart 
``` 

ทำการ Restart เครื่อง <br> 


#### ทำการดาวน์โหลดและติดตั้ง Linux kernel update package <br> 
### [Download](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi )

  

#### ทำการตั้งค่า WSL ค่าเริ่มต้นให้เป็น version 2 <br> 

เปิด PowerShell as Administrator and run: 

```shell 
wsl --set-default-version 2 
``` 

#### ทำการติดตั้ง Linux distribution 

ดาวน์โหลดได้จาก list นี้<br>
[Ubuntu 18.04 LTS](https://www.microsoft.com/store/apps/9N9TNGVNDL3Q)<br>
[Ubuntu 20.04 LTS](https://www.microsoft.com/store/apps/9n6svws3rx71)<br>
[Ubuntu 22.04 LTS](https://www.microsoft.com/store/apps/9PN20MSR04DW)<br>

จากนั้นกด Download หรือ Get เพื่อติดตั้ง

<img src="../image/get-ubuntu.png" alt="drawing" width="400"/>


ดาวน์โหลดเสร็จแล้ว กดที่ Launch ก็จะเข้าสู่หน้า cmd ของ Ubuntu ให้ทำการสร้าง User, Password ให้เรียบร้อย 

## ทำการติดตั้ง Docker Desktop 
 
### Download Docker Desktop
สามารถดาวน์โหลดได้จากลิงก์ด้านล่าง
### [Download](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?utm_source=docker&utm_medium=webreferral&utm_campaign=dd-smartbutton&utm_location=module&_gl=1*2931x0*_gcl_au*NzIwNjY0NTIxLjE3MjMxOTU0OTA.*_ga*MjExODE1OTc3OC4xNzIxMTE5ODQz*_ga_XJWPQMJYHQ*MTcyMzE5MzcxOS4yLjEuMTcyMzE5NTQ5NC41Ni4wLjA.)



### ทำการตั้ง Docker desktop ให้ใช้ WSL2

#### ไปที่เมนู Settings > Resources > WSL Integration เลือก Enable integration with my default WSL distro

<img src="../image/enable-wsl-docker-desktop.png" alt="drawing" width="400"/>

#### กด Apply & Restart 

### ทำการทดลองสร้าง Container 
 

เปิด PowerShell as Administrator and run: 
```sh
docker  run  -d  -p  80:80  docker/getting-started 
```
เสร็จแล้วทดสอบเข้า URL http://localhost 
 

### จบสำหรับการ ติดตั้ง Docker Desktop บน Windows เบื้องต้น