
# วิธีติดตั้ง Docker Desktop 🐳 บน Ubuntu  <br>

1. ทำการตั้งค่า Docker's apt repository <br>
```shell
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
2. ทำการติดตั้ง Docker packages. <br>
```shell
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
3. ทำการตรวจสอบว่าการติดตั้ง Docker Engine สำเร็จไหมโดยการรันอิมเมจ Hello-World <br>
```shell
sudo docker run hello-world 
```
## ทำการติดตั้ง Docker Desktop <br>

1. ทำการดาวโหลด DEB Package จาก https://docs.docker.com/desktop/install/ubuntu/ <br>
2. ติดตั้งแพ็คเกจด้วย apt ดังนี้ <br>
```shell
sudo apt-get update
sudo apt-get install ./docker-desktop-<arch>.deb
```
3. เปิด Docker Desktop  <br>
```shell
systemctl --user start docker-desktop
```
4. หากขึ้น Docker Desktop เป็นอันเสร็จสิ้นการติดตั้ง ✨ <br>
---------------------------
