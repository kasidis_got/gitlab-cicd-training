# ทดสอบสร้าง Milestone และ Issue บน Gitlab Repository ของตนเอง


## สร้าง Milestone โดยให้ระบุข้อมูลให้ครบถ้วนตามภาพ

![alt text](<image/Capto_Capture 2567-08-12_01-44-21_PM.png>)


![alt text](image/Capto_Annotation.png)

## สร้าง Issue อย่างน้อยสองอัน โดยให้ทดสอบสร้าง Tag และ อ้างอิงถึง Milestone ก่อนหน้านี้ตามภาพ

![alt text](<image/Capto_Capture 2567-08-12_11-15-11_PM.png>)

## ลองใช้ Issue boards สำหรับย้าย issue ต่างๆ

![alt text](<image/Capto_Capture 2567-08-12_11-18-23_PM.png>)

## ทดสอบปิด Issue ทั้งหมด และสังเกตที่ Milestone

![alt text](<image/Capto_Capture 2567-08-12_11-19-20_PM.png>)